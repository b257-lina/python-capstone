from django.urls import path
from . import views

# Adding a namespace to this urls.py helps django distinguish this set of routes from other urls.py files in other packages.
app_name = 'todolist'
urlpatterns = [
	path('', views.index, name="index"),
	# localhost:8000/todolist/1
	path('todos/<int:todoitem_id>/', views.todoitem, name="viewtodoitem"),
	path('register/', views.register, name="register"),
	path('change-password/', views.change_password, name="change_password"),
	path('login/', views.login_view, name="login"),
	path('logout/', views.logout_view, name="logout"),
	path('add_task/', views.add_task, name="add_task"),
	path('todos/<int:todoitem_id>/edit/', views.update_task, name="update_task"),
	path('todos/<int:todoitem_id>/delete/', views.delete_task, name="delete_task"),
	path('add_event/', views.add_event, name="add_event"),
	path('events/<int:events_id>/', views.events, name="viewevents"),
]